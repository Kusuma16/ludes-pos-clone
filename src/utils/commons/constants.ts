export const StatusCode = {
    InternalServerError: { code: 500, value: "InternalServerError" },
    Conflict: { code: 409, value: "Conflict" },
    NotFound: { code: 404, value: "NotFound" },
    Forbidden: { code: 403, value: "Forbidden" },
    Unauthorized: { code: 401, value: "Unauthorized" },
    BadRequest: { code: 400, value: "BadRequest" },
    OK: { code: 200, value: "OK" },
};

export const ResponseMessage = {
    Loaded: "Data berhasil ditampilkan",
    Added: "Data berhasil ditambah",
    Updated: "Data berhasil diupdate",
    Removed: "Data berhasil dihapus",
    FailLoaded: "Gagal menampilkan data",
    FailAdded: "Tambah data gagal",
    FailUpdated: "Edit data gagal",
    FailRemoved: "Hapus data gagal",
    UserNotFound: "User tidak ditemukan",
    EmailAlreadyExist: "Email sudah didaftarkan",
    LoginSuccess: "Login Berhasil",
    IncorrectEmailOrPassword: "Email atau Kata Sandi salah",
};
