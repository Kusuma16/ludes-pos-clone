import { 
    Controller,
    Get,
    Post,
    Put,
    Delete,
    Body,
    Param
} from '@nestjs/common';
import { StaffService } from './staff.service';
import { CreateStaffDto } from './dto/create-staff.dto';
import { StatusCode, ResponseMessage } from '../utils/commons/constants';

@Controller('staff')
export class StaffController {
    constructor(private service: StaffService) {}

    @Get()
    async getAll(){
        const staff = await this.service.getAll();
        return {
            staff,
            StatusCode: StatusCode.OK,
            message: ResponseMessage.Loaded,
        }
    }

    @Post()
    async create(@Body() createstaffdto: CreateStaffDto){
        const staff = await this.service.create(createstaffdto);
        return {
            staff,
            statusCode: StatusCode.OK,
            message: ResponseMessage.Added,
        }
    }

    @Put(':staff_id')
    async update(@Param('staff_id') staff_id, @Body() staff: CreateStaffDto){
        await this.service.update(staff_id, staff)
        return {
            statusCode: StatusCode.OK,
            message: ResponseMessage.Updated,
        }
    }

    @Delete(':staff_id')
    async delete(@Param('staff_id') staff_id: number) {
        await this.service.delete(staff_id);
        return {
            statusCode: StatusCode.OK,
            message: ResponseMessage.Removed
        }   
    }
}
