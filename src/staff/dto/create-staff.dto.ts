import {
    IsString,
    IsOptional,
    IsEnum,
} from "class-validator";

export enum StaffRole {
    edit      = 'edit',
    transaksi = 'transaksi',
    kasir     = 'kasir',
}

export class CreateStaffDto {

    @IsString()
    tanggal_lahir: string

    @IsString()
    pendidikan_terakhir: string

    @IsString()
    nomor_staff: string

    @IsString()
    jabatan: string

    @IsString()
    gaji: string

    @IsOptional()
    @IsEnum(StaffRole)
    hak_akses: StaffRole

}