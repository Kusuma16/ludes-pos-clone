import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Staff } from './entity/staff.entity';
import { CreateStaffDto } from './dto/create-staff.dto';

@Injectable()
export class StaffService {
    constructor(
        @InjectRepository(Staff) private stafRepository: Repository<Staff>,
    ) {}

    async getAll(): Promise<Staff[]> {
        return await this.stafRepository.find();
    }

    async create(CreateStaffDto: CreateStaffDto): Promise<Staff[]> {
        return await this.stafRepository.save(CreateStaffDto);
    }

    async update(staff_id: number, data: Partial<CreateStaffDto>) {
        this.stafRepository.update({ staff_id }, data);
    }

    async delete(staff_id: number) {
        this.stafRepository.delete(staff_id)
    }

}
