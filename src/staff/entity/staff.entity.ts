import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

enum StaffRole {
    edit      = 'edit',
    transaksi = 'transaksi',
    kasir     = 'kasir',
}

@Entity('staff')
export class Staff {
    @PrimaryGeneratedColumn()
    staff_id: number;

    @Column({ type: String, nullable: false, length: 255, unique: true })
    tanggal_lahir: Date

    @Column({ type: String, nullable: false, length: 255 })
    pendidikan_terakhir: string

    @Column({ type: String, nullable: false, length: 255  })
    nomor_staff: string;

    @Column({ type: String, nullable: false, length: 255  })
    jabatan: string;

    @Column({ type: String, nullable: false, length: 255 })
    gaji: string;

    @Column({ type: 'enum', enum: StaffRole, default: StaffRole.kasir })
    hak_akses: StaffRole;
}