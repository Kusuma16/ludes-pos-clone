import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StaffModule } from './staff/staff.module';
import { Staff } from './staff/entity/staff.entity';


@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'ludes_pos',
      entities: [Staff],
      synchronize: true,
    }),
    StaffModule
  ],
})
export class AppModule {}
